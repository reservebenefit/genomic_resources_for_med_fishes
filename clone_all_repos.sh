## nuclear genomes assembly
git clone https://gitlab.mbb.univ-montp2.fr/reservebenefit/genome_assemblies_collection.git
## RAD-seq processing
git clone https://gitlab.mbb.univ-montp2.fr/reservebenefit/snakemake_stacks2.git
## SNPs statistics
git clone https://gitlab.mbb.univ-montp2.fr/reservebenefit/snps_statistics.git
## mitochondrial genomes assembly
git clone https://gitlab.mbb.univ-montp2.fr/intrapop/assemble_mitogenome.git
